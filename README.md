# Vane Display

This project contains KiCad schematics and board layout for a driver for
electromechanical seven segment displays. The board is designed to carry
two [1" AlfaZeta digits](https://flipdots.com/en/products-services/small-7-segment-displays/). It allows control over serial or I2C connection.

## Status

Currently untested. Use at your own risk, or wait a few weeks for my boards
to arrive.

## License

![Creative Commons License](https://i.creativecommons.org/l/by/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
