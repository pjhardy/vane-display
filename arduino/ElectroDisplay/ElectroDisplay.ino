#include <EEPROM.h>
#include <SPI.h>
#include <WireS.h>

const byte shiftRegisterEnable = 5; // The G pin for the shift registers.
const byte shiftRegisterSelect = 8; // The SS pin for the shift register.

const int flipTime = 3; // How long to power a segment in ms

// digit patterns
// Segments in a shift register are arranged as follows:
// MSB 7 6 5 4 3 2 1 0 LSB
//       G F E D C B A
const byte digit3 = B01001111;
const byte digit8 = B01111111;
const byte digitClear  = B00000000;

byte digitBuffer[2]; // The digits we want to display.
byte spiBuffer[4]; // A buffer holding individual segments to flip.

// Indexes of different digits in spiBuffer
const uint8_t digit1Set = 0;
const uint8_t digit1Reset = 2;
const uint8_t digit2Set = 1;
const uint8_t digit2Reset = 3;
 
void setup() {
  REMAP |= (1<<SPIMAP); // Activate the remapped SPI pins.
  // Setting the output enable pin mode and driving it high to
  // disable the shift register outputs.
  // This should be done as early as possible.
  pinMode(shiftRegisterEnable, OUTPUT);
  digitalWrite(shiftRegisterEnable, HIGH);

  pinMode(shiftRegisterSelect, OUTPUT); // Setting up the SS pin.
  digitalWrite(shiftRegisterSelect, HIGH);
  SPI.begin();
  // The SPI library doesn't correctly set the
  // SPI pin mode when they're remapped, see
  // https://github.com/SpenceKonde/ATTinyCore/issues/117
  pinMode(9, OUTPUT);
  pinMode(7, OUTPUT);
  SPI.setClockDivider(SPI_CLOCK_DIV64);
}

void loop() {
  writeDigit(digit3, digit1Set, digit1Reset);
  delay(1000);
  writeDigit(digitClear, digit1Set, digit1Reset);
  delay(1000);
}

// Write a digit to the displays
// digitPattern: The byte pattern to write
// setIndex: The index of spiBuffer for the set register
// resetIndex: The index of spiBuffer for the reset register
void writeDigit(byte digitPattern, uint8_t setIndex, uint8_t resetIndex) {
  // VERY IMPORTANT:
  // Only ever set one bit of spiBuffer high at any one time.
  // In theory we can set a few, but let's walk first, k?
  for (int i=0; i<4; i++) {
    spiBuffer[i] = 0;
  }
  byte segmentReg = 0;
  int spiIndex = 0;
  for (int i=7; i>=0; i--) {
    segmentReg = 0;
    segmentReg |= 1 << i;
    // if i'th bit of the digitPattern is 1, then set it. Otherwise reset it.
    if ((digitPattern >> i) & 1) {
      spiIndex = setIndex;
    } else {
      spiIndex = resetIndex;
    }
    spiBuffer[spiIndex] = segmentReg;
    flipSegment();
    spiBuffer[spiIndex] = 0;
  }
}

void flipSegment() {
  // shift out the pattern to flip
  digitalWrite(shiftRegisterSelect, LOW);
  for (int i=0; i<4; i++) {
    SPI.transfer(spiBuffer[i]);
  }
  digitalWrite(shiftRegisterSelect, HIGH);
  // pulse shiftRegisterEnable to flip the segment
  digitalWrite(shiftRegisterEnable, LOW);
  delay(flipTime);
  digitalWrite(shiftRegisterEnable, HIGH);
}
